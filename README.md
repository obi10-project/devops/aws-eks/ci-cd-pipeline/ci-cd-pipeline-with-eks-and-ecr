#### Complete CI/CD Pipeline with EKS and AWS ECR

### Project Description
#### Create a private ECR repository
####  Create jenkins file to build and push Docker image to AWS ECR
#### Integrate deploying to Kubernetes cluster in the CI/CD pipeline from AWS ECR private registry

### The complete CI/CD project will have the following configuration:
#### 1. CI step increment version
#### 2. CI step build artifact for java maven application
#### 3. CI step build and push docker image to AWS ECR
#### 4. CD step deploy new application version to EKS cluster
#### 5. CD step commit the version update 